/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fungsi;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Owner
 */
public class WarnaTableRajal extends DefaultTableCellRenderer {
    public int statRawat = 10; 
    public int statRawatRegister = 19;
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (row % 2 == 1){
            component.setBackground(new Color(255,246,244));
        }else{
            component.setBackground(new Color(255,255,255));
        } 
        // edited by rsb1
        if(table.getValueAt(row, statRawat).toString().equals("Belum")) {
//            component.setBackground(new Color(0, 255, 255))
            component.setBackground(new Color(198, 235, 197)); //rgb(198, 235, 197)
//            component.setForeground(new Color(0, 0, 0));
        }
        if(table.getValueAt(row, statRawat).toString().equals("Batal")) {
            component.setBackground(new Color(255, 209, 209)); //;rgb(252, 255, 178) rgb(255, 209, 209)
//            component.setBackground(new Color(0, 128, 255));
//            component.setForeground(new Color(0, 0, 0));
        }
        if(table.getValueAt(row, statRawatRegister).toString().equals("Belum")) {
//            component.setBackground(new Color(0, 255, 255))
            component.setBackground(new Color(198, 235, 197)); //rgb(198, 235, 197)
//            component.setForeground(new Color(0, 0, 0));
        }
        if(table.getValueAt(row, statRawatRegister).toString().equals("Batal")) {
            component.setBackground(new Color(255, 209, 209)); //;rgb(252, 255, 178) rgb(255, 209, 209)
//            component.setBackground(new Color(0, 128, 255));
//            component.setForeground(new Color(0, 0, 0));
        }        

        
        return component;
    }

}
